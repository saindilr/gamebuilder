package com.example.blackjackv2

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_activity.*
import java.util.concurrent.ThreadLocalRandom

class MainActivity : AppCompatActivity() {
    var winningNumber=21
    var decks =1
    var inverseConditions =false
    var moneyOnHand=20000
    var bet =1000
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        setContentView(R.layout.activity_main)

        val conditions = arrayOf("Standard", "Inverse")
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, conditions)
        conditionOption.adapter = arrayAdapter

        conditionOption.onItemSelectedListener = object :

            AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                inverseConditions = position == 1
            }
        }

        playStandard.setOnClickListener {
            val intent = Intent(this@MainActivity, gameMain::class.java)
            intent.putExtra("winningNumber", 21)
            intent.putExtra("decks",1)
            intent.putExtra("inverseConditions",false)
            intent.putExtra("moneyOnHand",moneyOnHand)
            intent.putExtra("bet",bet)
            startActivity(intent)
        }
        //set decks
        setDecks.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater
            builder.setTitle("Set No of Decks")
            val dialogLayout = inflater.inflate(R.layout.dialog_activity, null)
            val input  = dialogLayout.findViewById<EditText>(R.id.inputQuery)
            builder.setView(dialogLayout)
            builder.setPositiveButton("OK") {
                    dialogInterface, i ->
                if(input.text.toString() !="") {
                    decks = input.text.toString().toInt()
                    decksMainView.setText("Number of Decks: " + decks.toString())
                }
            }
            builder.show()
        }
        setWinNo.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater
            builder.setTitle("Winning Number (5-25)")
            val dialogLayout = inflater.inflate(R.layout.dialog_activity, null)
            val input  = dialogLayout.findViewById<EditText>(R.id.inputQuery)
            builder.setView(dialogLayout)
            builder.setPositiveButton("OK") {
                    dialogInterface, i ->
                if(input.text.toString() !="") {
                    winningNumber = input.text.toString().toInt()
                    if(winningNumber>25){
                        winningNumber=25
                    }else if(winningNumber<5)
                    {
                        winningNumber=5
                    }
                    winMainView.setText("Winning Number: " + winningNumber.toString())
                }
            }
            builder.show()
        }
        setMoneyOnHand.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater
            builder.setTitle("Set Money On Hand")
            val dialogLayout = inflater.inflate(R.layout.dialog_activity, null)
            val input  = dialogLayout.findViewById<EditText>(R.id.inputQuery)
            builder.setView(dialogLayout)
            builder.setPositiveButton("OK") {
                    dialogInterface, i ->
                if(input.text.toString() !="") {
                    moneyOnHand = input.text.toString().toInt()
                    moneyText.setText("Cash On Hand: $" + moneyOnHand.toString())
                }
            }
            builder.show()
        }

        setBet.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater
            builder.setTitle("Set Bet Amount")
            val dialogLayout = inflater.inflate(R.layout.dialog_activity, null)
            val input  = dialogLayout.findViewById<EditText>(R.id.inputQuery)
            builder.setView(dialogLayout)
            builder.setPositiveButton("OK") {
                    dialogInterface, i ->
                if(input.text.toString() !="") {
                    if(input.text.toString().toInt()<moneyOnHand) {
                        bet = input.text.toString().toInt()
                        betText.setText("Bet: $" + bet.toString())
                    }else{
                        bet = 1000
                        betText.setText("Bet: $" + bet.toString())
                    }
                }
            }
            builder.show()
        }
        playCustom.setOnClickListener {
            val intent = Intent(this@MainActivity, gameMain::class.java)
            intent.putExtra("winningNumber", winningNumber)
            intent.putExtra("decks",decks)
            intent.putExtra("inverseConditions",inverseConditions)
            intent.putExtra("moneyOnHand",moneyOnHand)
            intent.putExtra("bet",bet)
            startActivity(intent)
        }
    }
}

