package com.example.blackjackv2

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_game_main.*
import java.util.concurrent.ThreadLocalRandom

class gameMain : AppCompatActivity() {

    //variable declarations
    val list = mutableListOf<String>()
    val hitlist = mutableListOf<String>()
    var count = 0
    var winningNumber =0
    var decks =0
    var inverseConditions= false
    var moneyOnHand=0
    var bet =0

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        setContentView(R.layout.activity_game_main)

        //get intent
        val intent = getIntent()
        winningNumber = intent.getIntExtra("winningNumber",21)
        decks =intent.getIntExtra("decks",1)
        inverseConditions =intent.getBooleanExtra("inverseConditions",false)
        moneyOnHand = intent.getIntExtra("moneyOnHand",20000)
        bet = intent.getIntExtra("bet",1000)
        winNumText.setText("Winning No: " + winningNumber.toString())
        deckNumText.setText("No of Decks: " + decks.toString())
        moneyOnHandView.setText("Money On Hand: $" + moneyOnHand.toString())
        betView.setText("Bet: $" + bet.toString())

        var cond =""
        if(inverseConditions)
        {
            cond="Inverse"
        }else{
            cond="Standard"
        }
        conditionText.setText("Conditions: " + cond)

        resetButton.setOnClickListener {
            list.clear()
            count = 0
            resetCards()
            startRound.isEnabled = true
            startRound.isClickable = true
        }
        startRound.setOnClickListener {
            resetCards()
            startRound.isEnabled = false

            Handler().postDelayed({
                changeCards(adminCard1)
            }, 500)
            Handler().postDelayed({
                changeCards(player1Card1)
            }, 1500)

            Handler().postDelayed({
                changeCards(player1Card2)
            }, 2500)

            Handler().postDelayed({
                changeCards(adminCard2)

            }, 3500)
            Handler().postDelayed({
                startRound.isEnabled = true
                Log.d("genRam", "Generated Cards: " + count.toString())
                checkwin()
                hasWon()
                displayOptions()
            }, 4500)

        }
        hitCard.setOnClickListener {
            if(!hasWon()) {
                hitCards(player1Card3)
            }
        }

        doubleBet.setOnClickListener {
            if(!hasWon())
            {
                bet = bet*2
                betView.setText("Bet: $" + bet.toString() )
                doubleBet.isEnabled=false
            }
        }
        passCard.setOnClickListener {
            var i =adminSum.text.toString().toInt()

            if (!hasWon()){
                hitCards(adminCard3)
            }
        }
    }

    fun displayOptions()
    {
        hitCard.setVisibility(View.VISIBLE)
        doubleBet.setVisibility(View.VISIBLE)
        passCard.setVisibility(View.VISIBLE)
        hitCard.isClickable=true
        doubleBet.isClickable =true
        passCard.isClickable=true
    }

    fun checkwin(){
        if(!hasWon()) {
            val a = adminSum.text.toString().toInt()
            val pl1 = player1Sum.text.toString().toInt()
            if (a > winningNumber) {
                player1Sum.setText(player1Sum.text.toString() + " (Won)")
            } else if (pl1 > winningNumber) {
                adminSum.setText(adminSum.text.toString() + " (Won)")
            } else if (pl1 == winningNumber) {
                player1Sum.setText(player1Sum.text.toString() + " (Won)")
            } else if (a == winningNumber) {
                adminSum.setText(adminSum.text.toString() + " (Won)")
            }
        }
    }

    fun resetCards() {
        hitlist.clear()
        adminSum.text ="0"
        player1Sum.text = "0"
        adminCard1.setImageResource(R.drawable.card_back)
        adminCard2.setImageResource(R.drawable.card_back)
        adminCard3.setImageResource(R.drawable.card_back)
        adminCard4.setImageResource(R.drawable.card_back)
        adminCard5.setImageResource(R.drawable.card_back)
        player1Card1.setImageResource(R.drawable.card_back)
        player1Card2.setImageResource(R.drawable.card_back)
        player1Card3.setImageResource(R.drawable.card_back)
        player1Card4.setImageResource(R.drawable.card_back)
        player1Card5.setImageResource(R.drawable.card_back)
        player1Card3.setVisibility(View.INVISIBLE)
        player1Card4.setVisibility(View.INVISIBLE)
        player1Card5.setVisibility(View.INVISIBLE)
        adminCard3.setVisibility(View.INVISIBLE)
        adminCard4.setVisibility(View.INVISIBLE)
        adminCard5.setVisibility(View.INVISIBLE)
        hitCard.setVisibility(View.INVISIBLE)
        hitCard.isEnabled=true
        doubleBet.isEnabled=true
        passCard.isEnabled=true
        doubleBet.setVisibility(View.INVISIBLE)
        passCard.setVisibility(View.INVISIBLE)
    }

    fun cardDone(cds: MutableList<String>,card:String):Boolean
    {
        var ct =0
        for(i in cds)
        { if(i==card) { ct++ } }
        if(ct==decks)
        { Log.d("done","card done: " + card.toString() )
            return true
        }else { return false } }

    fun generateRandomCard(): String {
        var found = false
        while (!found) {
            val rndOne = ThreadLocalRandom.current().nextInt(1, 14)
            val rndTwo = ThreadLocalRandom.current().nextInt(1, 5)
            if (count == 52*decks) {
                Log.d("genRam", "no more cards: " + count.toString())
                found = true
                startRound.isClickable = false
                startRound.isEnabled = false
            } else if (!(cardDone(list,rndOne.toString() + "_" + rndTwo.toString()))) {
                list.add(rndOne.toString() + "_" + rndTwo.toString())
                count++
                found = true

                if (rndOne < 10) {
                    val cardN = "0" + rndOne.toString() + "_" + rndTwo.toString()
                    return cardN
                } else {
                    val cardN = rndOne.toString() + "_" + rndTwo.toString()
                    return cardN
                }
            }
        }

        return "0"
    }

    fun changeCards(img: ImageView):Int {
        var num = generateRandomCard()
        var score =0
        Log.d("genRam", num)
        if (count != 52*decks) {
            val drawableResourceId = this.resources.getIdentifier(
                "@drawable/c" + num.toString(),
                "drawable",
                this.packageName
            )
            img.setImageResource(drawableResourceId)


            if(img==adminCard1)
            {
                var i = num.toString().substring(0, 2).toInt()
                if(i>10) {
                    i=10
                }
                score+=i
                adminSum.text = i.toString()
            }else if(img == adminCard2)
            {
                var i = adminSum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                adminSum.text = i.toString()
            }else if(img == adminCard3)
            {
                var i = adminSum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                adminSum.text = i.toString()
            }else if(img == adminCard4)
            {
                var i = adminSum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                adminSum.text = i.toString()
            }else if(img == adminCard5)
            {
                var i = adminSum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                adminSum.text = i.toString()
            }else if (img == player1Card1) {
                var i = num.toString().substring(0, 2).toInt()
                if(i>10) {
                    i=10
                }
                score+=i

                player1Sum.text = i.toString()
            } else if (img == player1Card2) {

                var i = player1Sum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                player1Sum.text = i.toString()
            }
            else if (img == player1Card3) {

                var i = player1Sum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                player1Sum.text = i.toString()
            }
            else if (img == player1Card4) {

                var i = player1Sum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                player1Sum.text = i.toString()
            } else if (img == player1Card5) {

                var i = player1Sum.text.toString().toInt()
                var o =num.toString().substring(0, 2).toInt()
                if(o>10) {
                    o=10
                }
                i = i+o
                score+=i
                player1Sum.text = i.toString()
            }
        }
        return score
    }

    fun hasWon(): Boolean
    {
        if(player1Sum.text.toString().contains("Won"))
        {
            hitCard.isEnabled=false
            doubleBet.isEnabled=false
            passCard.isEnabled=false
            moneyOnHand=moneyOnHand+bet
            moneyOnHandView.setText("Money On Hand: $" + (moneyOnHand).toString())
            return true
        }else if(adminSum.text.toString().contains("Won"))
        {
            hitCard.isEnabled=false
            doubleBet.isEnabled=false
            passCard.isEnabled=false
            moneyOnHand=moneyOnHand-bet
            moneyOnHandView.setText("Money On Hand: $" + (moneyOnHand).toString())
            return true
        }
        return false
    }

    fun hitCards(img: ImageView)
    {


        if(img==player1Card3 && !hitlist.contains("player1Card3")) {
            player1Card3.setVisibility(View.VISIBLE)
            Handler().postDelayed({
                changeCards(player1Card3)
                hitlist.add("player1Card3")
                checkwin()
                hasWon()
            }, 1000)
        }
        else if(img==player1Card3 && hitlist.contains("player1Card3") && !hitlist.contains("player1Card4") ) {
            player1Card4.setVisibility(View.VISIBLE)
            Handler().postDelayed({
                changeCards(player1Card4)
                hitlist.add("player1Card4")
                checkwin()
                hasWon()
            }, 1000)
        } else if(img==player1Card3 && hitlist.contains("player1Card3") && hitlist.contains("player1Card4") && !hitlist.contains("player1Card5") ) {
            player1Card5.setVisibility(View.VISIBLE)
            Handler().postDelayed({
                changeCards(player1Card5)
                hitlist.add("player1Card5")
                checkwin()
                hasWon()
            }, 1000)
        }else  if(img==adminCard3 && !hitlist.contains("adminCard3")) {
            adminCard3.setVisibility(View.VISIBLE)
            Handler().postDelayed({
                changeCards(adminCard3)
                hitlist.add("adminCard3")
                checkwin()
                hasWon()
            }, 1000)
        }else  if(img==adminCard3 && hitlist.contains("adminCard3") && !hitlist.contains("adminCard4")) {
            adminCard4.setVisibility(View.VISIBLE)
            Handler().postDelayed({
                changeCards(adminCard4)
                hitlist.add("adminCard4")
                checkwin()
                hasWon()
            }, 1000)
        }else {
            if(img==adminCard3 && hitlist.contains("adminCard3") && hitlist.contains("adminCard4") && !hitlist.contains("adminCard5")) {
                adminCard5.setVisibility(View.VISIBLE)
                Handler().postDelayed({
                    changeCards(adminCard5)
                    hitlist.add("adminCard3")
                    checkwin()
                    hasWon()
                }, 1000)
            }
        }

    }
}

